package com.guilhermebisotto.dojo_1.data

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class AuthInterceptor : Interceptor {

    private var authentication: String = ""

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val authenticatedRequest = request.newBuilder()
            .header("Content-Type", "application/json")
            .build()
        return chain.proceed(authenticatedRequest)
    }

    fun changeAuthentication(auth: String) {
        authentication = auth
    }
}
