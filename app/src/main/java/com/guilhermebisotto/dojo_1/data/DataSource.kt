package com.guilhermebisotto.dojo_1.data

import com.guilhermebisotto.dojo_1.data.models.StarWarsResult
import retrofit2.Response

interface DataSource {

    interface Remote {
        suspend fun getStarship(): Response<StarWarsResult?>
    }
}