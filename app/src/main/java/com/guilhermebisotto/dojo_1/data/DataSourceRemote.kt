package com.guilhermebisotto.dojo_1.data

import com.guilhermebisotto.dojo_1.data.models.StarWarsResult
import retrofit2.Response

class DataSourceRemote : DataSource.Remote {

    private val api = RetrofitInitializer().apiService()

    override suspend fun getStarship(): Response<StarWarsResult?> = request(api.getStarshipsAsync())
}