package com.guilhermebisotto.dojo_1.data

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.IO
import kotlinx.coroutines.withContext
import retrofit2.Response

suspend fun <T> request(getRequest: Deferred<Response<T?>>) =
    withContext(Dispatchers.IO) {
        getRequest.await()
    }