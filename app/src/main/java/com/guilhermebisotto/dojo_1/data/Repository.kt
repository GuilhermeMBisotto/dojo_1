package com.guilhermebisotto.dojo_1.data

class Repository {

    private val remote = DataSourceRemote()

    suspend fun getStarships() = remote.getStarship().body()
}