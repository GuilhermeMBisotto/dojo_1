package com.guilhermebisotto.dojo_1.data.models

data class StarWarsResult(
    val count: Int,
    val next: String?,
    val previous: String?,
    val results: List<Starship>
)