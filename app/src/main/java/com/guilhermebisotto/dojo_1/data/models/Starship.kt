package com.guilhermebisotto.dojo_1.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Starship(
    @SerializedName("starship_class") val starshipClass: String? = null,
    @SerializedName("cost_in_credits") val costInCredits: String? = null,
    @SerializedName("max_atmosphering_speed") val maxAtmospheringSpeed: String? = null,
    @SerializedName("hyperdrive_rating") val hyperdriveRating: String? = null,
    @SerializedName("MGLT") val mglt: String? = null,
    @SerializedName("cargo_capacity") val cargoCapacity: String? = null,
    val photo: String = "https://s3.amazonaws.com/dx-libs.4all.com/guild-android/images/death_star.jpg",
    val name: String? = null,
    val model: String? = null,
    val manufacturer: String? = null,
    val length: String? = null,
    val crew: String? = null,
    val passengers: String? = null,
    val consumables: String? = null,
    val url: String? = null,
    val created: String? = null,
    val edited: String? = null,
    val films: List<String>? = null,
    val pilots: List<String>? = null
) : Parcelable