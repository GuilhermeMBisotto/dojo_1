package com.guilhermebisotto.dojo_1.data.services

import com.guilhermebisotto.dojo_1.data.models.StarWarsResult
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface APIService {

    @GET("starships")
    fun getStarshipsAsync(): Deferred<Response<StarWarsResult?>>
}