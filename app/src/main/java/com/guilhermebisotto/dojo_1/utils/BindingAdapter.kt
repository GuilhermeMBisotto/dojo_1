package com.guilhermebisotto.dojo_1.utils

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.guilhermebisotto.dojo_1.R

@BindingAdapter("loadImage")
fun ImageView.loadImage(url: String?) {
    Glide.with(context)
        .load(url)
        .apply(
            RequestOptions()
                .centerCrop()
                .error(R.drawable.ic_launcher_background)
        )
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(this)
}

@BindingAdapter("app:name", "app:model")
fun TextView.loadText(name: String?, model: String?) {
    text = context.getString(
        R.string.item_starship_name_model,
        name,
        model
    )
}