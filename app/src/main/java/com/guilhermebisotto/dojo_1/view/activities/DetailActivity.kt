package com.guilhermebisotto.dojo_1.view.activities

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.guilhermebisotto.dojo_1.R
import com.guilhermebisotto.dojo_1.data.models.Starship
import com.guilhermebisotto.dojo_1.databinding.ActivityDetailBinding
import com.guilhermebisotto.dojo_1.utils.Constants
import com.guilhermebisotto.dojo_1.viewmodels.DetailActivityViewModel
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    private val viewModel by lazy {

        ViewModelProviders.of(this)[DetailActivityViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityDetailBinding>(this,
            R.layout.activity_detail
        )

        val starship = intent.getParcelableExtra<Starship>(Constants.starshipExtra)

        binding.viewModel = viewModel

        viewModel.setStarship(starship)

        checkBoxFavoriteStarship.setOnCheckedChangeListener { _, isChecked ->

            viewModel.setStarshipFavorite(isChecked)
        }
    }
}