package com.guilhermebisotto.dojo_1.view.activities

import android.content.Intent
import android.os.Bundle
import android.transition.TransitionManager
import androidx.core.util.Pair
import android.view.View
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.guilhermebisotto.dojo_1.R
import com.guilhermebisotto.dojo_1.databinding.ActivityDetailBinding
import com.guilhermebisotto.dojo_1.databinding.ActivityMainBinding
import com.guilhermebisotto.dojo_1.utils.Constants
import com.guilhermebisotto.dojo_1.view.adapters.HomeAdapter
import com.guilhermebisotto.dojo_1.viewmodels.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val viewModel: MainActivityViewModel by lazy {
        ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
    }

    private val adapter: HomeAdapter by lazy {
        HomeAdapter(mutableListOf()) { starship, imageView ->

            val intent = Intent(this, DetailActivity::class.java).apply {
                putExtra(Constants.starshipExtra, starship)
            }

            val comp: Pair<View, String> = Pair(imageView, "imageTransition")
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                comp
            )

            startActivity(intent, options.toBundle())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this,
            R.layout.activity_main
        )

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        //Site da APi
        //https://swapi.co

        configureObservables()
        configureSwipeToRefresh()
        configureRecyclerView()
        configureExtendedFab()
        configureSearch()
        viewModel.getStarships()
    }

    private fun configureObservables() {

        viewModel.starships.observe(this, Observer {

            swipeRefreshLayoutsHome.isRefreshing = false

            adapter.updateItems(it.results)

            TransitionManager.beginDelayedTransition(swipeRefreshLayoutsHome)

            animationView.visibility = View.GONE

            rvHome.visibility = View.VISIBLE
        })
    }

    private fun configureRecyclerView() {
        rvHome.layoutManager = LinearLayoutManager(this)

        rvHome.setHasFixedSize(true)

        rvHome.adapter = adapter

        rvHome.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (!recyclerView.canScrollVertically(1)) {

                    efabHome.visibility = View.VISIBLE
                } else {

                    efabHome.visibility = View.GONE
                }
            }
        })
    }

    private fun configureSwipeToRefresh() {

        swipeRefreshLayoutsHome.setOnRefreshListener {

            swipeRefreshLayoutsHome.isRefreshing = false

            TransitionManager.beginDelayedTransition(swipeRefreshLayoutsHome)

            rvHome.visibility = View.GONE

            animationView.visibility = View.VISIBLE

            viewModel.getStarships()
        }
    }

    private fun configureExtendedFab() {
        efabHome.setOnClickListener {
            efabHome.visibility = View.GONE
            rvHome.scrollToPosition(0)
        }
    }

    private fun configureSearch() {


        viewModel.inputText.observe(this, Observer {
            adapter.setTextFilter(it)

        })
//        svHome.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
//            override fun onQueryTextSubmit(query: String?): Boolean {
//                return true
//            }
//
//            override fun onQueryTextChange(newText: String?): Boolean {
//                adapter.setTextFilter(newText)
//                return true
//            }
//        })
    }
}
