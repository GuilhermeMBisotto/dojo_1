package com.guilhermebisotto.dojo_1.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.guilhermebisotto.dojo_1.R
import com.guilhermebisotto.dojo_1.data.models.Starship
import com.guilhermebisotto.dojo_1.databinding.ItemHomeBinding
import kotlinx.android.synthetic.main.item_home.view.*

class HomeAdapter(
    private var items: MutableList<Starship>,
    private val clickListener: (Starship, ImageView) -> Unit
) : RecyclerView.Adapter<HomeAdapter.DojoViewHolder>() {

    private var itemsCopy: MutableList<Starship> = mutableListOf()

    fun updateItems(list: List<Starship>) {
        items.clear()
        items.addAll(list)
        itemsCopy.clear()
        itemsCopy.addAll(list)
        notifyDataSetChanged()
    }

    private fun noFilterList() {
        items.clear()
        items.addAll(itemsCopy)
        notifyDataSetChanged()
    }

    private fun filteredList(filter: String) {
        items.clear()
        items.addAll(itemsCopy.filter {
            it.name?.contains(filter, ignoreCase = true) ?: false
        })
        notifyDataSetChanged()
    }

    fun setTextFilter(filter: String?) {
        if (filter.isNullOrBlank()) {
            noFilterList()
        } else {
            filteredList(filter)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DojoViewHolder {

        val view = DataBindingUtil.inflate<ItemHomeBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_home,
            parent,
            false
        )

        return DojoViewHolder(view, clickListener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: DojoViewHolder, position: Int) {

        holder.bind(items[position])
    }

    class DojoViewHolder(val binding: ItemHomeBinding, val clickListener: (Starship, ImageView) -> Unit) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(starship: Starship) {

            binding.starship = starship

            binding.onClick = View.OnClickListener {
                clickListener(starship, binding.ivItemHomePhoto)
            }
        }
    }
}