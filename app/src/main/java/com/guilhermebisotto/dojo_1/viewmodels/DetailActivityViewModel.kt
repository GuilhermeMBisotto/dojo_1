package com.guilhermebisotto.dojo_1.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.guilhermebisotto.dojo_1.data.models.Starship

class DetailActivityViewModel : ViewModel() {

    private val mutableStarship = MutableLiveData<Starship>()

    var starshipFavorite = false
        private set

    fun getStarship() = mutableStarship.value

    fun setStarship(starship: Starship) {

        mutableStarship.value = starship
    }

    fun setStarshipFavorite(isFavorite: Boolean) {

        starshipFavorite = isFavorite
    }
}