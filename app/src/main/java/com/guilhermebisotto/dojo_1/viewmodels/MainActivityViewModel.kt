package com.guilhermebisotto.dojo_1.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.guilhermebisotto.dojo_1.data.Repository
import com.guilhermebisotto.dojo_1.data.models.StarWarsResult
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.IO
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class MainActivityViewModel : ViewModel(), CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.IO

    private val repository = Repository()
    private val _starships = MutableLiveData<StarWarsResult>()
    val starships = Transformations.map(_starships) { it }

    fun getStarships() = launch {
        _starships.postValue(repository.getStarships())
    }

    val inputText = MutableLiveData<String>()
}